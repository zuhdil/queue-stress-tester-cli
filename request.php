#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

$command = new class() extends Command
{
    protected function configure()
    {
        $this->setName('request')
             ->setDescription('Send an HTTP request')
             ->addArgument('uri', InputArgument::REQUIRED, 'URI address')
             ->addOption('method', 'X', InputOption::VALUE_OPTIONAL, 'HTTP method', 'GET');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $method = $input->getOption('method');
        $uri = $input->getArgument('uri');

        $client = new Client();
        $response = $client->request($method, $uri);

        $output->writeln(Psr7\str($response));
    }
};

$application = new Application($command->getName(), '0.1.0');
$application->add($command);
$application->setDefaultCommand($command->getName(), true);
$application->run();
