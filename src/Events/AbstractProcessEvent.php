<?php
namespace ParallelRequests\Events;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Process\Process;

abstract class AbstractProcessEvent extends Event
{
    /**
     * @var Process
     */
    private $process;

    /**
     * @var float
     */
    private $duration;

    public function __construct(Process $process, float $startedAt)
    {
        $this->process = $process;
        $this->duration = microtime(true) - $startedAt;
    }

    public function getProcess(): Process
    {
        return $this->process;
    }

    public function getDuration(): float
    {
        return $this->duration;
    }
}
