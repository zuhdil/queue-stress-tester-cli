<?php
namespace ParallelRequests\Events;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Process\Process;

class ProcessUpdateEvent extends AbstractProcessEvent
{
    /**
     * @var string
     */
    private $output;

    /**
     * @var string
     */
    private $type;

    public function __construct(Process $process, float $startedAt, string $output, string $type)
    {
        parent::__construct($process, $startedAt);

        $this->output = $output;
        $this->type = $type;
    }

    public function getOutput(): string
    {
        return $this->output;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
