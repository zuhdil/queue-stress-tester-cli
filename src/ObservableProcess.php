<?php
namespace ParallelRequests;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Process\Process;

class ObservableProcess
{
    /**
     * @var Process
     */
    private $process;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @var float
     */
    private $startedAt;

    public function __construct(Process $process, EventDispatcherInterface $dispatcher = null)
    {
        $this->process = $process;
        $this->dispatcher = $dispatcher ?: new EventDispatcher();
    }

    public function addListener(string $eventName, callable $listener, $priority = 0): self
    {
        $this->dispatcher->addListener($eventName, $listener, $priority);

        return $this;
    }

    public function addSubscriber(EventSubscriberInterface $subscriber): self
    {
        $this->dispatcher->addSubscriber($subscriber);

        return $this;
    }

    public function start(): self
    {
        if (!$this->process->isRunning()) {
            $this->startedAt = microtime(true);

            $this->dispatch(new Events\ProcessStartEvent($this->process, $this->startedAt));

            $this->process->start(function ($type, $data) {
                foreach (explode("\n", $data) as $line) {
                    if (mb_strlen($line) < 1) {
                        continue;
                    }
                    $this->dispatch(new Events\ProcessUpdateEvent($this->process, $this->startedAt, $line, $type));
                }
            });
        }

        return $this;
    }

    public function poll(): bool
    {
        if (!$this->process->isStarted()) {
            return false;
        }

        if ($this->process->isRunning()) {
            return true;
        }

        if ($this->process->isSuccessful()) {
            $this->dispatch(new Events\ProcessSuccessEvent($this->process, $this->startedAt));
        } else {
            $this->dispatch(new Events\ProcessFailureEvent($this->process, $this->startedAt));
        }

        return false;
    }

    private function dispatch(Event $event)
    {
        $this->dispatcher->dispatch(get_class($event), $event);
    }
}
