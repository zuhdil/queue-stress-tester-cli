<?php
namespace ParallelRequests;


class RunProcessesPerMinuteListener
{
    private $processFactory;
    private $interval;
    private $nextTick;

    public function __construct(ProcessFactory $processFactory, int $procPerMinute = 0)
    {
        $this->processFactory = $processFactory;
        $this->interval = $procPerMinute !== 0 ? (60*1000000) / $procPerMinute : 1000.0;
    }

    public function __invoke(TimedProcessRunner $runner, float $startedAt)
    {
        $duration = microtime(true) - $startedAt;
        $timestamp = $duration * 1000000.0;

        if (is_null($this->nextTick) || $timestamp >= $this->nextTick) {
            $runner->add($this->processFactory->create($startedAt));
            $this->nextTick = $timestamp + $this->interval;
        }
    }
}
