<?php
namespace ParallelRequests;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Process\Process;

class TimedProcessRunner
{
    const NO_MAX = -1;

    /**
     * @var int
     */
    private $duration;

    /**
     * @var EventDispatcher
     */
    private $dispatcher;

    /**
     * @var ObservableProcess[]
     */
    private $processes = [];

    /**
     * @var ObservableProcess[]
     */
    private $running = [];

    /**
     * @var ObservableProcess[]
     */
    private $waiting = [];

    /**
     * @var int
     */
    private $maxSimultaneous = -1;

    public function __construct(
        int $durationInSeconds,
        int $maxSimultaneous = self::NO_MAX,
        EventDispatcherInterface $dispatcher = null
    ) {
        $this->duration = $durationInSeconds;
        $this->maxSimultaneous = $maxSimultaneous;
        $this->dispatcher = $dispatcher ?: new EventDispatcher();
    }

    public function setMaxSimultaneous(int $maxSimultaneous): self
    {
        $this->maxSimultaneous = $maxSimultaneous;

        return $this;
    }

    public function addListener(string $eventName, callable $listener, $priority = 0): self
    {
        $this->dispatcher->addListener($eventName, $listener, $priority);

        return $this;
    }

    public function addSubscriber(EventSubscriberInterface $subscriber): self
    {
        $this->dispatcher->addSubscriber($subscriber);

        return $this;
    }

    public function add(ObservableProcess $process): self
    {
        $this->processes[] = $process;
        $this->startProcess($process);

        return $this;
    }

    public function addProcess(Process $process): self
    {
        return $this->add(new ObservableProcess($process, $this->dispatcher));
    }

    public function run(callable $onProgress, float $refreshRate = 1000): void
    {
        $startedAt = microtime(true);
        $stopAt = time() + $this->duration;

        do {
            $timestamp = time();

            if ($timestamp < $stopAt) {
                $onProgress($this, $startedAt);
            }

            usleep($refreshRate);
        }
        while ($this->poll() || $timestamp < $stopAt);
    }

    private function poll(): bool
    {
        $this->running = array_filter(
            $this->running,
            function (ObservableProcess $process) {
                return $process->poll();
            }
        );

        $this->checkFinished();

        return $this->isRunning();
    }

    private function checkFinished()
    {
        if ($this->maxSimultaneous !== static::NO_MAX
            && count($this->waiting) > 0
            && count($this->running) < $this->maxSimultaneous) {
            for ($i = count($this->running); $i < $this->maxSimultaneous && count($this->waiting) > 0; $i++) {
                $process = array_shift($this->waiting);
                $process->start();
                $this->running[] = $process;
            }
        }
    }

    private function isRunning(): bool
    {
        return count($this->running) > 0;
    }

    private function startProcess(ObservableProcess $process): void
    {
        if ($this->maxSimultaneous === static::NO_MAX || count($this->running) < $this->maxSimultaneous) {
            $process->start();
            $this->running[] = $process;
        } else {
            $this->waiting[] = $process;
        }
    }
}
