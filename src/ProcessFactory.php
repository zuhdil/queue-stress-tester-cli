<?php
namespace ParallelRequests;

interface ProcessFactory
{
    function create(float $runnerStartedAt);
}
