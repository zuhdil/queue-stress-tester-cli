<?php
namespace ParallelRequests;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;

class RunnerLineProgressSubscriber implements EventSubscriberInterface
{
    private $output;
    private $label;
    private $runnerStartedAt;

    public function __construct(ConsoleOutputInterface $output, float $runnerStartedAt, string $label)
    {
        $this->output = $output;
        $this->runnerStartedAt = $runnerStartedAt;
        $this->label = $label;
    }

    public static function getSubscribedEvents()
    {
        return [
            Events\ProcessStartEvent::class => 'onProcessStart',
            Events\ProcessUpdateEvent::class => 'onProcessUpdate',
            Events\ProcessSuccessEvent::class => 'onProcessSuccess',
            Events\ProcessFailureEvent::class => 'onProcessFailure',
        ];
    }

    public function onProcessStart(Events\ProcessStartEvent $event)
    {
        $this->output->writeln(sprintf(
            '<comment>%6.2fs:</comment> %s - <fg=blue>→ Starting...</>',
            microtime(true) - $this->runnerStartedAt,
            $this->label
        ));
    }

    public function onProcessUpdate(Events\ProcessUpdateEvent $event)
    {
        $this->output->writeln(sprintf(
            '<comment>%6.2fs:</comment> %s - %s: %s (%5.2fs)',
            microtime(true) - $this->runnerStartedAt,
            $this->label,
            $event->getType(),
            $event->getOutput(),
            $event->getDuration()
        ));
    }

    public function onProcessSuccess(Events\ProcessSuccessEvent $event)
    {
        $this->output->writeln(sprintf(
            '<comment>%6.2fs:</comment> %s - <info>✓ Succeeded</info> (%5.2fs)',
            microtime(true) - $this->runnerStartedAt,
            $this->label,
            $event->getDuration()
        ));
    }

    public function onProcessFailure(Events\ProcessFailureEvent $event)
    {
        $process = $event->getProcess();
        $this->output->writeln(sprintf(
            '<comment>%6.2fs:</comment> %s - <error>x Failed</error> (code: %d) %s (%5.2fs)',
            microtime(true) - $this->runnerStartedAt,
            $this->label,
            $process->getExitCode(),
            $process->getExitCodeText(),
            $event->getDuration()
        ));
        $this->output->writeln((new ProcessFailedException($process))->getMessage());
    }
}
