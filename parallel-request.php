#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use ParallelRequests\ObservableProcess;
use ParallelRequests\ProcessFactory;
use ParallelRequests\RunnerLineProgressSubscriber;
use ParallelRequests\RunProcessesPerMinuteListener;
use ParallelRequests\TimedProcessRunner;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;


$command = new class() extends Command
{
    protected function configure()
    {
        $this->setName('parallel-request')
             ->setDescription('Send an HTTP request')
             ->addOption('time', 't', InputOption::VALUE_OPTIONAL, 'Duration in seconds', 60)
             ->addOption('rpm', 'r', InputOption::VALUE_OPTIONAL, 'Requests per minute', 60)
             ->addOption('maxproc', 'p', InputOption::VALUE_OPTIONAL, 'Maximum simultaneous processes', TimedProcessRunner::NO_MAX)
             ->addOption('method', 'X', InputOption::VALUE_OPTIONAL, 'HTTP method, default to the GET method', 'GET')
             ->addArgument('uri', InputArgument::REQUIRED, 'URI address');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $durationInSeconds = $input->getOption('time');
        $requestPerMinute = $input->getOption('rpm');
        $maxSimultaneousProcesses = $input->getOption('maxproc');
        $method = $input->getOption('method');
        $uri = $input->getArgument('uri');

        $processFactory = new class($method, $uri, $output) implements ProcessFactory
        {
            private $colours = ['red', 'green', 'blue', 'yellow', 'magenta', 'white', 'cyan'];
            private $commandPath;
            private $index = 0;
            private $method;
            private $uri;
            private $output;

            public function __construct(string $method, string $uri, OutputInterface $output)
            {
                $this->output = $output;
                $this->method = $method;
                $this->uri = $uri;
                $this->commandPath = realpath(__DIR__.'/request.php');
            }

            public function create(float $runnerStartedAt): ObservableProcess
            {
                $this->index++;
                $label = $this->getLabel($this->index);
                $command = $this->getCommand($this->index);
                $process = new ObservableProcess(new Process($command));
                $process->addSubscriber(
                    new RunnerLineProgressSubscriber($this->output, $runnerStartedAt, $label)
                );

                return $process;
            }

            private function getLabel(int $index): string
            {
                $colour = $this->colours[$index % count($this->colours)];

                return sprintf('<options=bold;fg=%s>%s</>', $colour, 'request-'.$index);
            }

            private function getCommand(int $index): string
            {
                $commandStr = sprintf('php %s -X %s "%s"', $this->commandPath, $this->method, $this->uri);

                return str_replace('{index}', $index, $commandStr);
            }
        };

        (new TimedProcessRunner($durationInSeconds, $maxSimultaneousProcesses))
            ->run(new RunProcessesPerMinuteListener($processFactory, $requestPerMinute));
    }
};


$application = new Application($command->getName(), '0.1.0');
$application->add($command);
$application->setDefaultCommand($command->getName(), true);
$application->run();
